from django.shortcuts import render

from .models import Estadio, Equipo , Jugador

def index(request):
	return render(request , 'app_nflp/index.html' , {})

# • Lista General de Jugadores
def list_jugadores(request):
	queryset = Jugador.objects.all()
	context = {
	"queryset" : queryset,
	}
	return render(request , "app_nflp/list_jugadores.html" , context)

# • Lista Genereal de Equipos
def list_equipos(request):
	queryset = Equipo.objects.all()
	context = {
	"queryset" : queryset,
	}
	return render(request , "app_nflp/list_equipos.html" , context)
# • Lista General de Estadios

def list_estadios(request):
	queryset = Estadio.objects.all()
	context = {
	"queryset" : queryset,
	}
	return render(request , "app_nflp/list_estadios.html" , context)
# • Detalle de Jugadores

def detail_jugador(request , id):
	queryset = Jugador.objects.get(id = id)	
	context = {
		"queryset":queryset
	}
	return render(request,"app_nflp/detail_jugador.html", context)
# • Detalle de Estadios
def detail_estadio(request , id):
	queryset = Estadio.objects.get(id = id)	
	context = {
		"queryset":queryset
	}
	return render(request,"app_nflp/detail_estadio.html", context)

# • Detalle de Equipos
def detail_equipo(request , id):
	queryset = Equipo.objects.get(id = id)	
	context = {
		"queryset":queryset
	}
	return render(request,"app_nflp/detail_equipo.html", context)

# • Lista de Jugadores de un equipo proporcionado por el usuario
def list_juxequipo(request , equipo):
	queryset = Jugador.objects.filter(equipo = equipo)	
	context = {
		"queryset":queryset
	}
	return render(request,"app_nflp/list_juxequipo.html", context)
# • Lista de todos los jugadores de una posicion especificada por el usuario

def posiciones(request):
	return render(request , "app_nflp/posiciones.html" , {})

def list_juxposicion(request , posicion):
	queryset = Jugador.objects.filter(posicion = posicion)	
	context = {
		"queryset":queryset
	}
	return render(request,"app_nflp/list_juxposicion.html", context)
# • Lista de Jugadores intercambiados a otros equipos

def list_juintercambiados(request):
	queryset = Jugador.objects.filter(intercambiado = True)	
	context = {
		"queryset":queryset
	}
	return render(request,"app_nflp/list_juintercambiados.html", context)

# • Lista de Jugadores despedidos del equipo

def list_judespedidos(request , id):
	queryset = Jugador.objects.filter(contratado = False , equipo = id)	
	context = {
		"queryset":queryset
	}
	return render(request,"app_nflp/list_judespedidos.html", context)