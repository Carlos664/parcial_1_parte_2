from django.contrib import admin

from .models import Jugador, Equipo , Estadio

# Register your models here.
#admin.site.register(Jugador)
#admin.site.register(Equipo)
#admin.site.register(Estadio)

@admin.register(Jugador)
class JugadorAdmin(admin.ModelAdmin):
    list_display = (
 	"nombre",
 	"apellido",
 	"posicion",
 	"equipo"
 	 )

@admin.register(Equipo)
class EquipoAdmin(admin.ModelAdmin):
    list_display = (
 	"nombre",
 	"estadio",
 	"campeonatos"
 	 )

@admin.register(Estadio)
class EstadioAdmin(admin.ModelAdmin):
    list_display = (
 	"nombre",
 	"ciudad",
 	"capacidad"
 	 )