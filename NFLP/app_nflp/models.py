from django.db import models

# Create your models here.

class Estadio(models.Model):
	nombre = models.CharField(max_length = 30 )
	ciudad = models.CharField(max_length = 30)
	capacidad = models.IntegerField(default = 30000)
	def __str__(self):
		return self.nombre


class Equipo(models.Model):
	nombre = models.CharField(max_length = 30)
	estadio = models.OneToOneField(Estadio, on_delete = models.CASCADE)
	campeonatos = models.IntegerField()


	def __str__(self):
		return self.nombre

class Jugador(models.Model):
	nombre = models.CharField(max_length = 30)
	apellido = models.CharField(max_length = 30)
	edad = models.IntegerField()
	peso = models.FloatField()
	altura = models.FloatField()
	numero = models.IntegerField()
	equipo = models.ForeignKey(Equipo, on_delete = models.CASCADE)
	posicion = models.CharField(max_length = 30 , choices = [("Quarterback","Quarterback "),
		("Corredor","Corredor"),("Defensa","Defensa"),("Receptor","Receptor")])
	contratado = models.BooleanField(default = True)
	intercambiado = models.BooleanField(default = False)
	equipoAnterior = models.CharField(max_length = 30 , default = 'NA')
	slug = models.CharField(max_length = 30)

	def __str__(self):
		return self.nombre


