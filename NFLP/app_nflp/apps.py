from django.apps import AppConfig


class AppNflpConfig(AppConfig):
    name = 'app_nflp'
