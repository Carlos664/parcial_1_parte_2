from django.urls import path
from app_nflp import views 


urlpatterns = [
    path('', views.index , name = 'index'),
    path('posiciones/', views.index , name = 'posiciones'),
    path('list_jugadores/', views.list_jugadores , name = 'list_jugadores'),
	path('list_equipos/', views.list_equipos , name = 'list_equipos'),
    path('list_estadios/', views.list_estadios , name = 'list_estadios'),
 	path('detail_jugador/<int:id>', views.detail_jugador , name = 'detail_jugador'),
 	path('detail_estadio/<int:id>', views.detail_estadio , name = 'detail_estadio'),
 	path('detail_equipo/<int:id>', views.detail_equipo , name = 'detail_equipo'),
 	path('list_juxequipo/<int:equipo>', views.list_juxequipo , name = 'list_juxequipo'),
 	path('list_juxposicion/<str:posicion>', views.list_juxposicion , name = 'list_juxposicion'),
 	path('list_juintercambiados/', views.list_juintercambiados , name = 'list_juintercambiados'),
 	path('list_judespedidos/<int:id>/', views.list_judespedidos , name = 'list_judespedidos'),

]


